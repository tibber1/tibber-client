import { ImageText } from "./ImageText";
import styles from "../styles/Home.module.css";

export const ImageTextColumns: React.FC = () => {
  return (
    <div className={styles["image-columns"]}>
      <ImageText
        src={"/assets/aircondition-woman.jpg"}
        width={"100%"}
        height={"100%"}
      ></ImageText>
      <ImageText
        src={"/assets/yard.jpg"}
        width={266}
        height={178}
        text={{ title: "Direct mailing strategy buzz social proof" }}
      ></ImageText>
      <ImageText
        src={"/assets/tesla.jpg"}
        width={266}
        height={178}
        text={{ title: "Hypotheses value proposition" }}
      ></ImageText>
    </div>
  );
};
