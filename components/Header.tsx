import styles from "../styles/Home.module.css";

export const Header: React.FC = () => {
  return (
    <header className={styles.header}>
      <img src="/assets/Logo.png" />
    </header>
  );
};
