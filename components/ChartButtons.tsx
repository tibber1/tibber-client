import { Button } from "./Button";
import styles from "../styles/Home.module.css";

type ChartButtonsProps = {
  showTrick: () => void;
  reset: () => void;
}

export const ChartButtons: React.FC<ChartButtonsProps> = (chartButtons) => {
  return (
    <div className={styles["chart-buttons"]}>
      <Button
        handleClick={chartButtons.showTrick}
        text="Show me a trick"
        color="primary"
      ></Button>
      <Button handleClick={chartButtons.reset} text="Reset" color="white"></Button>
    </div>
  );
};
