import React from "react";
import {
  ResponsiveContainer,
  Line,
  LineChart,
  XAxis,
  YAxis,
  AreaChart,
  Area,
  BarChart,
  Bar,
  ComposedChart,
  ScatterChart,
  Scatter,
} from "recharts";

import { Weather } from "../interfaces";
import {
  timeFormatted,
  temperatureFormatted,
} from "../utilities/weather-utils";
import styles from "../styles/Home.module.css";

type WeatherChartProps = {
  weather: Weather;
  trickNumber: number;
};

export const WeatherChart: React.FC<WeatherChartProps> = ({
  weather,
  trickNumber,
}) => {
  const chooseTrickNumber = (trick: number) => {
    switch (trick) {
      case 0: {
        return lineChart;
      }
      case 1: {
        return areaChart;
      }
      case 2: {
        return barChart;
      }
      case 3: {
        return areaGradientChart;
      }
      case 4: {
        return composedChart;
      }
      case 5: {
        return scatterChart;
      }
      default: {
        return lineChart;
      }
    }
  };

  const xAxis = (
    <XAxis
      dataKey="time"
      minTickGap={50}
      stroke={"rgba(255, 255, 255, 0.6)"}
      tickFormatter={timeFormatted}
    />
  );

  const yAxis = (
    <YAxis
      axisLine={false}
      stroke={"rgba(255, 255, 255, 0.6)"}
      tickFormatter={temperatureFormatted}
      tickMargin={2015}
      tickSize={-2000}
    />
  );

  const lineChart = (
    <LineChart data={weather.entries}>
      <Line
        dataKey="temperature"
        dot={false}
        stroke={"rgba(255, 255, 255, 0.8)"}
        type="natural"
      />
      {xAxis}
      {yAxis}
    </LineChart>
  );

  const areaChart = (
    <AreaChart data={weather.entries}>
      {xAxis}
      {yAxis}
      <Area
        type="monotone"
        dataKey="temperature"
        stroke="#666666"
        fillOpacity={1}
        fill={"rgba(24,97,112,0.7)"}
      />
    </AreaChart>
  );

  const areaGradientChart = (
    <AreaChart data={weather.entries}>
      <defs>
        <linearGradient id="colorTemp" x1="0" y1="0" x2="0" y2="1">
          <stop offset="5%" stopColor="#45b9d1" stopOpacity={0.9} />
          <stop offset="95%" stopColor="#45b9d1" stopOpacity={0.1} />
        </linearGradient>
      </defs>
      {xAxis}
      {yAxis}
      <Area
        type="monotone"
        dataKey="temperature"
        stroke="#666666"
        fillOpacity={1}
        fill="url(#colorTemp)"
      />
    </AreaChart>
  );

  const barChart = (
    <BarChart data={weather.entries}>
      {xAxis}
      {yAxis}
      <Bar dataKey="temperature" fill="#45b9d1" />
    </BarChart>
  );

  const composedChart = (
    <ComposedChart data={weather.entries}>
      {xAxis}
      {yAxis}
      <Area
        type="monotone"
        dataKey="temperature"
        stroke="#666666"
        fillOpacity={1}
        fill={"rgba(24,97,112,0.7)"}
      />
      <Line
        dataKey="temperature"
        dot={false}
        stroke={"rgba(255, 255, 255, 0.8)"}
        type="natural"
      />
      <Bar dataKey="temperature" fill="#45b9d1" />
    </ComposedChart>
  );

  const scatterChart = (
    <ScatterChart data={weather.entries}>
      {xAxis}
      {yAxis}
      <Scatter dataKey="temperature" fill="#44c8b3" />
    </ScatterChart>
  );

  return (
    <div className={styles.chart}>
      <ResponsiveContainer width="100%" height="100%">
        {chooseTrickNumber(trickNumber % 6)}
      </ResponsiveContainer>
    </div>
  );
};
