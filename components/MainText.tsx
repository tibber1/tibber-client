import styles from "../styles/Home.module.css";

export const MainText: React.FC = () => {
  return (
    <div className={styles["main-text"]}>
      <h3 className={styles["main-text-header"]}>
        Infrastructure supply chain seed lean startup technology
      </h3>
      <p className={styles["main-text-paragraph"]}>
        Assets traction angel investor user experience social media leverage
        value proposition startup success founders creative. Equity value
        proposition launch party business-to-consumer research & development
        freemium bandwidth stock scrum project analytics.
      </p>
      <p className={styles["main-text-paragraph"]}>
        Agile development backing business-to-consumer analytics burn rate
        leverage business-to-business market creative responsive web design
        graphical user interface
      </p>
    </div>
  );
};
