import styles from "../styles/Home.module.css";

type ButtonProps = {
  text: string;
  color: string;
  handleClick: (param: any) => void;
};

export const Button: React.FC<ButtonProps> = (button) => {
  return (
    <button
      className={
        button.color === "primary"
          ? `${styles.button} ${styles["button-primary"]}`
          : `${styles.button} ${styles["button-white"]}`
      } onClick={button.handleClick}
    >
      {button.text}
    </button>
  );
};
