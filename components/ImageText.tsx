import styles from "../styles/Home.module.css";

type ImageTextProps = {
  width: number | string;
  height: number | string;
  src: string;
  text?: {
    title: string;
    content?: string;
  };
};

export const ImageText: React.FC<ImageTextProps> = (imageText) => {
  return (
    <div className={styles["image-text"]} style={{ width: imageText.width }}>
      <img
        src={imageText.src}
        width={imageText.width}
        height={imageText.height}
      />
      {imageText.text && (
        <div>
          <h4>{imageText.text.title}</h4>
          <p>{imageText.text.content}</p>
        </div>
      )}
    </div>
  );
};
