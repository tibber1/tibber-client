const API_URL = process.env.DEVELOPER_SERVER;

export async function fetchAPI(url: string) {
  const headers = { "Content-Type": "application/json" };

  const res = await fetch(API_URL + url, {
    method: "GET",
    headers,
  });

  const json = await res.json();
  if (json.errors) {
    console.error(json.errors);
    throw new Error("Failed to fetch API");
  }
  return json.data;
}

export async function getWeatherData() {
  const res = await fetchAPI("home");
  return res;
}
