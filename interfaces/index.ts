export type Weather = {
  minTemperature: number;
  maxTemperature: number;
  entries: Array<WeatherEntry>;
}

export type WeatherEntry = {
  time: string;
  temperature: number;
  type: string;
}