import Head from "next/head";
import styles from "../styles/Home.module.css";
import { WeatherChart } from "../components/WeatherChart";
import { Header } from "../components/Header";
import { ChartButtons } from "../components/ChartButtons";
import { GetStaticProps } from "next";
import { getWeatherData } from "../lib/api";
import { MainText } from "../components/MainText";
import { ImageTextColumns } from "../components/ImageTextColumns";
import { useState } from "react";

export default function Home({ weather }) {
  const [trick, showTrick] = useState(0);

  return (
    <div className={styles.container}>
      <Head>
        <title>Weathber App</title>
        <link rel="icon" href="/favicon.ico" />
        <link
          rel="preload"
          href="/fonts/avenir-next/avenir-next-regular.ttf"
          as="font"
          crossOrigin=""
        />
        <link
          rel="preload"
          href="/fonts/silka/silka-bold.ttf"
          as="font"
          crossOrigin=""
        />
      </Head>

      <Header></Header>
      <main className={styles.main}>
        <h3 className={styles.title}>Todays Temperature</h3>
        <WeatherChart trickNumber={trick} {...weather}></WeatherChart>
        <ChartButtons
          showTrick={() => showTrick(trick + 1)}
          reset={() => showTrick(0)}
        ></ChartButtons>
        <MainText></MainText>
        <ImageTextColumns></ImageTextColumns>
      </main>
    </div>
  );
}

export const getStaticProps: GetStaticProps = async () => {
  const res = await getWeatherData();

  return {
    props: {
      weather: res.me.home,
    },
  };
};
