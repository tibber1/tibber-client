import { TickFormatterFunction } from "recharts";

export const timeFormatted: TickFormatterFunction = (date: string) => {
  const dateObject = new Date(date);
  const hh = dateObject.getHours().toString().padStart(2, "0");
  const mm = dateObject.getMinutes().toString().padStart(2, "0");
  return `${hh}:${mm}`;
};

export const temperatureFormatted: TickFormatterFunction = (
  temperature: string
) => {
  return `${temperature} C`;
};
